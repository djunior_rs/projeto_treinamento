program Project1;

uses
  Vcl.Forms,
  UILogin in 'UILogin.pas' {Form1},
  UIPrincipal in 'UIPrincipal.pas' {Form2},
  UIPDV in 'UIPDV.pas' {Form3},
  UIProduto in 'UIProduto.pas' {Form4},
  UIClientes in 'UIClientes.pas' {Form5},
  UIFornecedores in 'UIFornecedores.pas' {Form6},
  UIUsuarios in 'UIUsuarios.pas' {Form7},
  UICompras in 'UICompras.pas' {Form8},
  UIVendas in 'UIVendas.pas' {Form9},
  URSProdutoClass in 'URSProdutoClass.pas' {Form10},
  URSClienteClass in 'URSClienteClass.pas' {Form11},
  URSUsuariosClass in 'URSUsuariosClass.pas' {Form15},
  URSFornecedorClass in 'URSFornecedorClass.pas' {Form16};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TForm9, Form9);
  Application.CreateForm(TForm10, Form10);
  Application.CreateForm(TForm11, Form11);
  Application.CreateForm(TForm15, Form15);
  Application.CreateForm(TForm16, Form16);
  Application.Run;
end.
