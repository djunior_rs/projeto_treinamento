object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Form7'
  ClientHeight = 518
  ClientWidth = 776
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RsDBGrid1: TRsDBGrid
    Left = 8
    Top = 80
    Width = 753
    Height = 424
    Cursor = crHandPoint
    FixedColor = 15853535
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    CharNotAllowed = '|[]'
    XLSFileDir = 'c:\'
    DefaultRowHeight = 17
    RSOptions = [ColunaReindexar]
    AllowAppend = False
    Zebra = True
    ShowScrowBar = True
    ZebraFontColor = clWindowText
  end
  object RSButton1: TRSButton
    Left = 8
    Top = 8
    Width = 113
    Height = 49
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'INCLUIR'
    TabOrder = 1
  end
  object RSButton2: TRSButton
    Left = 127
    Top = 8
    Width = 113
    Height = 49
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'EDITAR'
    TabOrder = 2
  end
  object RSButton3: TRSButton
    Left = 246
    Top = 8
    Width = 113
    Height = 49
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'EXCLUIR'
    TabOrder = 3
  end
end
