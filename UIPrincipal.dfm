object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 498
  ClientWidth = 816
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 138
    Height = 24
    Caption = 'FERRAMENTAS:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object RSButton1: TRSButton
    Left = 16
    Top = 57
    Width = 145
    Height = 33
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'CLIENTES'
    TabOrder = 0
  end
  object RSButton2: TRSButton
    Left = 16
    Top = 96
    Width = 145
    Height = 34
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'PRODUTOS'
    TabOrder = 1
  end
  object RSButton3: TRSButton
    Left = 16
    Top = 136
    Width = 145
    Height = 34
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'VENDAS'
    TabOrder = 2
  end
  object RSButton4: TRSButton
    Left = 16
    Top = 176
    Width = 145
    Height = 34
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'FORNECEDORES'
    TabOrder = 3
  end
  object RSButton5: TRSButton
    Left = 16
    Top = 216
    Width = 145
    Height = 33
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'VENDAS'
    TabOrder = 4
  end
  object RSButton6: TRSButton
    Left = 16
    Top = 255
    Width = 145
    Height = 34
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'COMPRAS'
    TabOrder = 5
  end
end
