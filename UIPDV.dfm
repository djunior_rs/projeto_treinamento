object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 519
  ClientWidth = 827
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 47
    Width = 101
    Height = 19
    Caption = 'Busca Produto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 543
    Top = 47
    Width = 61
    Height = 19
    Caption = 'Carrinho'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 391
    Top = 8
    Width = 49
    Height = 33
    Caption = 'PDV'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 184
    Top = 96
    Width = 31
    Height = 13
    Caption = 'Label4'
  end
  object Label5: TLabel
    Left = 352
    Top = 130
    Width = 100
    Height = 16
    Caption = 'VALOR PRODUTO'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 352
    Top = 194
    Width = 74
    Height = 16
    Caption = 'tOTAL ITENS'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 352
    Top = 258
    Width = 65
    Height = 16
    Caption = 'DESCONTO'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 352
    Top = 386
    Width = 82
    Height = 16
    Caption = 'VALOR TOTAL'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 352
    Top = 322
    Width = 105
    Height = 16
    Caption = 'VALOR ACR'#201'CIMO'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object RSButton1: TRSButton
    Left = 728
    Top = 480
    Width = 91
    Height = 31
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'ADMIN'
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 16
    Top = 72
    Width = 505
    Height = 33
    TabOrder = 1
    Text = 'Edit1'
  end
  object RsDBGrid1: TRsDBGrid
    Left = 543
    Top = 72
    Width = 276
    Height = 385
    FixedColor = 15853535
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    CharNotAllowed = '|[]'
    XLSFileDir = 'c:\'
    DefaultRowHeight = 17
    RSOptions = [ColunaReindexar]
    AllowAppend = False
    Zebra = True
    ShowScrowBar = True
    ZebraFontColor = clWindowText
  end
  object Edit2: TEdit
    Left = 352
    Top = 152
    Width = 169
    Height = 33
    TabOrder = 3
    Text = 'Edit1'
  end
  object Edit3: TEdit
    Left = 352
    Top = 216
    Width = 169
    Height = 33
    TabOrder = 4
    Text = 'Edit1'
  end
  object Edit4: TEdit
    Left = 352
    Top = 280
    Width = 169
    Height = 33
    TabOrder = 5
    Text = 'Edit1'
  end
  object Edit5: TEdit
    Left = 352
    Top = 408
    Width = 169
    Height = 33
    TabOrder = 6
    Text = 'Edit1'
  end
  object Edit6: TEdit
    Left = 352
    Top = 344
    Width = 169
    Height = 33
    TabOrder = 7
    Text = 'Edit1'
  end
  object RSButton2: TRSButton
    Left = 8
    Top = 480
    Width = 109
    Height = 31
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'FINALIZAR VENDA'
    TabOrder = 8
  end
  object RSButton3: TRSButton
    Left = 136
    Top = 480
    Width = 109
    Height = 31
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    Caption = 'CANCELAR VENDA'
    TabOrder = 9
  end
end
